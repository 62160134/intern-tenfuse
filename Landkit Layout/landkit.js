var slideIndex  = 1;

function changeImg(n) {
  showDiv(slideIndex += n);
}

function showDiv(n) {
  var i;
  var x = document.getElementsByClassName("slide-img");
  var text = document.getElementsByClassName("slide-card-text");
  console.log(x);
  console.log(text);
  if(n > x.length) {
    slideIndex = 1;
  }
  if(n < 1){
    slideIndex = x.length;
  }
  for(i = 0; i < x.length; i++) {
    x[i].style.display = "none";
    text[i].style.display = "none"
  }
  x[slideIndex-1].style.display = "block";
  text[slideIndex-1].style.display = "flex";
}