const body = document.querySelector("body");
var mainContainer = body.querySelector(".main-container-collapsed");
var bottomSidebarIcon = document.getElementById("bt-menu-icon");
var check = false;

function collapsedMenu() {
  if(check == false) {
    bottomSidebarIcon.style.transform = "rotate(180deg)";   
    check = true;
  }else if(check == true) {
    bottomSidebarIcon.style.transform = "rotate(0deg)";   
    check = false
  }

  if(mainContainer.classList.contains("main-container-collapsed")) {
    mainContainer.classList.toggle("main-container-expand");
  }else {
    mainContainer.classList.toggle("main-container-collapsed");
  }
}

// Tooltip
var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-tooltip="tooltip"]'))
var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
  return new bootstrap.Tooltip(tooltipTriggerEl)
})

var popoverTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="popover"]'))
var popoverList = popoverTriggerList.map(function (popoverTriggerEl) {
  return new bootstrap.Popover(popoverTriggerEl)
})

const toastTrigger = document.getElementById('liveToastBtn')
const toastLiveExample = document.getElementById('liveToast')
if (toastTrigger) {
  toastTrigger.addEventListener('click', () => {
    const toast = new bootstrap.Toast(toastLiveExample)

    toast.show()
  })
}